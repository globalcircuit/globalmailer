<p align="center">
    <h1 align="center">GlobalMailer Extension for Yii 2</h1>
    <br>
</p>

This extension provides a [GlobalMailer](https://globalcircuit.co/) mail solution for [Yii framework 2.0](http://www.yiiframework.com).

For license information check the [LICENSE](LICENSE.md)-file.


Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist globalcircuit/yii2-globalmailer
```


to the require section of your composer.json.

> Note: Version requires PHP .

Usage
-----

To use this extension,  simply add the following code in your application configuration:

```php
return [
    //....
    'components' => [
        'mailer' => [
            'class' => 'globalcircuit\globalMailer\Mailer',
        ],
    ],
];
```

You can then send an email as follows:

```php
Yii::$app->mailer->compose('contact/html')
     ->setFrom('from@domain.com')
     ->setTo($form->email)
     ->setSubject($form->subject)
     ->send();
```


