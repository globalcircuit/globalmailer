<?php

namespace globalcircuit\globalmailer;

use Yii;
use yii\base\InvalidConfigException;
use yii\mail\BaseMailer;

/**
 *
 * To use Mailer, you should configure it in the application configuration like the following:
 *
 * ```php
 * [
 *     'components' => [
 *         'mailer' => [
 *             'class' => 'globalcircuit\globalmailer\Mailer',
 *             'transport' => [
 *                 'host' => 'localhost',
 *                 'username' => 'username',
 *                 'password' => 'password',
 *             ],
 *         ],
 *         // ...
 *     ],
 *     // ...
 * ],
 * ```
 *
 *
 * To send an email, you may use the following code:
 *
 * ```php
 * Yii::$app->mailer->compose('contact/html', ['contactForm' => $form])
 *     ->setFrom('from@domain.com')
 *     ->setTo($form->email)
 *     ->setSubject($form->subject)
 *     ->send();
 * ```
 *
 *
 */
class Mailer extends BaseMailer
{
    /**
     * @var string message default class name.
     */
    public $messageClass = 'globalcircuit\globalmailer\Message';
    /**
     * @var bool whether to enable writing of the internal logs using Yii log mechanism.
     * @see Logger

     */
    public $enableMailerLogging = false;

    /**
     * @var uri for send mail.
     */
    public $uri;
    public $service;
    public $bcc;

    public function send($message){
        $address = $message->getTo();
        if (is_array($address)) {
            $address = implode(', ', array_keys($address));
        }
        $subject = utf8_decode($message->subject);

        $ch = curl_init();
        $fields = "mensaje=". base64_encode($message->htmlBody). "&asunto=".$subject."&destinatarios=".$address."&uri=".$this->uri."&destinatariosBCC=".$this->bcc;
        curl_setopt($ch, CURLOPT_URL, $this->service);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$fields);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $server_output = curl_exec ($ch);
        curl_close ($ch);
        return $server_output;
    }

    /**
     * @inheritdoc
     */
    protected function sendMessage($message)
    {
        /* @var $message Message */
        $address = $message->getTo();
        if (is_array($address)) {
            $address = implode(', ', array_keys($address));
        }
        Yii::info('Sending email "' . $message->getSubject() . '" to "' . $address . '"', __METHOD__);

        return $this->getSwiftMailer()->send($message->getSwiftMessage()) > 0;
    }


}
