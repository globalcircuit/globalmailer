<?php

namespace globalcircuit\globalmailer;

use Yii;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\mail\BaseMessage;


class Message  extends BaseMessage
{

    private $_message;

    private $_from;
    private $_to;
    private $_cc;
    private $_subject;
    /**
     * @var string The Text content of the email body. Substitutions allowed: `Dear -name-,\nThis is the message..`
     */
    public $textBody;

    /**
     * @var string The HTML content of the email body. Substitutions allowed: `Dear -name-,<br>This is the message..`
     */
    public $htmlBody;

    /**
     * @inheritdoc
     */
    public function getFrom()
    {
        return $this->_from;
    }

    /**
     * @inheritdoc
     */
    public function setFrom($from)
    {
        $this->_from = $from;
        return $this;
    }


    /**
     * @inheritdoc
     */
    public function getTo()
    {
        return $this->_to;
    }

    /**
     * @inheritdoc
     */
    public function setTo($to)
    {
        $this->_to = $to;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCc()
    {
        return $this->_cc;
    }

    /**
     * @inheritdoc
     */
    public function setCc($cc)
    {
        $this->_cc = $cc;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getSubject()
    {
        return $this->_subject;
    }

    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setHtmlBody($html)
    {
        $this->htmlBody = $html;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function setTextBody($text)
    {
        $this->textBody = $text;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getCharset()
    {
        return $this->charset;
    }

    /**
     * @inheritdoc
     */
    public function setCharset($charset)
    {
        $this->charset = $charset;
        return $this;
    }


    /**
     * @inheritdoc
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @inheritdoc
     */
    public function setReplyTo($replyTo)
    {
        $this->replyTo = $replyTo;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @inheritdoc
     */
    public function setBcc($bcc)
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function attach($fileName, array $options = [])
    {
        $this->attachments[] = ['file' => $fileName, 'options' => $options];
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function attachContent($content, array $options = [])
    {
        Yii::warning('attachContent is not implemented', self::LOGNAME);
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function embed($fileName, array $options = [])
    {
        Yii::warning('embed is not implemented', self::LOGNAME);
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function embedContent($content, array $options = [])
    {
        Yii::warning('embedContent is not implemented', self::LOGNAME);
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function toString()
    {
        return json_encode($this->buildMessage(), JSON_PRETTY_PRINT);
    }

}
